package pl.grmn.fakegps;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;


import pl.grmn.fakegps.models.Simulation;
import pl.grmn.fakegps.models.SimulationPoint;

import static android.widget.Toast.makeText;

public class MockLocationService extends IntentService {
	private boolean bStop = false;
	LocationManager locationManager;
	private Context appContext;

	/**
	 * A constructor is required, and must call the super IntentService(String)
	 * constructor with a name for the worker thread.
	 */
	public MockLocationService() {
		super("MockLocationService");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		appContext=getBaseContext();//Get the context here

		Intent notificationIntent = new Intent(this, MockLocationService.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

		Notification notification = new Notification.Builder(this)
				.setContentTitle("Simulation running")
				.setContentText("miau")
				.setContentIntent(pendingIntent)
				.build();

		startForeground(1, notification);

		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		return super.onStartCommand(intent, flags, startId);
	}

	/**
	 * The IntentService calls this method from the default worker thread with
	 * the intent that started the service. When this method returns, IntentService
	 * stops the service, as appropriate.
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		Log.v("miau", "Service intent received");
		Simulation simulation = (Simulation) intent.getSerializableExtra("simulation");
		Log.v("miau", simulation.toString());


		runSimulation(simulation);
	}

	public IBinder onBind(Intent intent)
	{
		return null;
	}

	public void runSimulation(Simulation simulation){
		try {
			locationManager.addTestProvider(LocationManager.GPS_PROVIDER,
					"requiresNetwork" == "",
					"requiresSatellite" == "",
					"requiresCell" == "",
					"hasMonetaryCost" == "",
					true,
					"supportsSpeed" == "",
					"supportsBearing" == "",
					android.location.Criteria.POWER_LOW,
					android.location.Criteria.ACCURACY_FINE);
		}catch (SecurityException exc){
			if(null !=appContext) {
				Handler handler = new Handler(Looper.getMainLooper());
				handler.post(new Runnable() {
					@Override
					public void run() {
						makeText(appContext, "You need to enable mock locations to play a simulation!", Toast.LENGTH_LONG).show();
					}
				});
			}
			return;
		}


		int size = simulation.getSimulationPoints().size();
		for (int i = 0; i < size; ++i) {
			SimulationPoint simulationPoint = simulation.getSimulationPoints().get(i);


			Location newLocation = new Location(LocationManager.GPS_PROVIDER);

			newLocation.setLatitude(simulationPoint.getLat());
			newLocation.setLongitude(simulationPoint.getLng());
			newLocation.setAccuracy(simulationPoint.getAcc());
			newLocation.setSpeed(simulationPoint.getSpeed());
			newLocation.setAltitude(simulationPoint.getAlt());
			newLocation.setBearing(simulationPoint.getBearing());

			this.setMock(newLocation);

			if(this.bStop)
				return;
			SystemClock.sleep((long)simulationPoint.getInterval());
		}
	}

	private void setMock(Location newLocation) {
		newLocation.setTime(System.currentTimeMillis());
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			newLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
		}

		locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);

		locationManager.setTestProviderStatus(LocationManager.GPS_PROVIDER,
				LocationProvider.AVAILABLE,
				null,System.currentTimeMillis());

		locationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER, newLocation);
	}

	@Override
	public void onDestroy() {
		Log.v("miau", "Service destroyed");
		Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
		this.bStop = true;
		this.stopSelf();
	}
}
