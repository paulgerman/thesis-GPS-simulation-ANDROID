package pl.grmn.fakegps.providers;

import pl.grmn.fakegps.providers.response.SimulationResponse;
import pl.grmn.fakegps.providers.response.TrackResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;



public interface ApiInterface {

    @GET("thesisEndpoint")
    Observable<SimulationResponse> getSimulation(@Query("q") String q);

    @GET("thesisEndpoint")
    Observable<TrackResponse> getTrack(@Query("q") String q);

}
