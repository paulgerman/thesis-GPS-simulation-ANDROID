package pl.grmn.fakegps.providers.response;

import pl.grmn.fakegps.models.Simulation;



public class SimulationResponse {
    public Simulation result;
    public String jsonrpc;
}
