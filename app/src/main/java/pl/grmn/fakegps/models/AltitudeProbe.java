package pl.grmn.fakegps.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AltitudeProbe implements Serializable {
    @SerializedName("elevation") private float elevation;
    @SerializedName("resolution") private float resolution;
    @SerializedName("location") private Point location;

    public float getElevation() {
        return elevation;
    }

    public void setElevation(float elevation) {
        this.elevation = elevation;
    }

    public float getResolution() {
        return resolution;
    }

    public void setResolution(float resolution) {
        this.resolution = resolution;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }
}
