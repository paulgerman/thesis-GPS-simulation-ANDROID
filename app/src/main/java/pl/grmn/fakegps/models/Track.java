package pl.grmn.fakegps.models;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

public class Track extends SugarRecord implements Serializable {
    @SerializedName("track_id") private String trackId;
    @SerializedName("user_id") private String userId;
    @SerializedName("track_name") private String trackName;
    @SerializedName("track_length_m") private String trackLengthMeters;
    @SerializedName("track_created_time") private String createdAt;

    @SerializedName("track_data_vertices") private List<Point> pointsList;
    @SerializedName("track_data_altitude_probes") private List<AltitudeProbe> altitudeProbesList;

    @SerializedName("track_preview_url") private String trackPreviewURL;

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackLengthMeters() {
        return trackLengthMeters;
    }

    public void setTrackLengthMeters(String trackLengthMeters) {
        this.trackLengthMeters = trackLengthMeters;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<Point> getPointsList() {
        return pointsList;
    }

    public void setPointsList(List<Point> pointsList) {
        this.pointsList = pointsList;
    }

    public List<AltitudeProbe> getAltitudeProbesList() {
        return altitudeProbesList;
    }

    public void setAltitudeProbesList(List<AltitudeProbe> altitudeProbesList) {
        this.altitudeProbesList = altitudeProbesList;
    }

    public String getTrackPreviewURL() {
        return trackPreviewURL;
    }

    public void setTrackPreviewURL(String trackPreviewURL) {
        this.trackPreviewURL = trackPreviewURL;
    }
}
