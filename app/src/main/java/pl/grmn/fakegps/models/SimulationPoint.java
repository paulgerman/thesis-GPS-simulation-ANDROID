package pl.grmn.fakegps.models;

import java.io.Serializable;

public class SimulationPoint extends Point implements Serializable {
	private float acc;
	private float dist;
	private float alt;
	private float alt_acc;
	private float bearing;
	private float bearing_acc;
	private float speed;
	private float speed_acc;
	private float interval;


	public float getInterval() {
		return interval;
	}

	public void setInterval(float interval) {
		this.interval = interval;
	}


	public float getAcc() {
		return acc;
	}

	public void setAcc(float acc) {
		this.acc = acc;
	}

	public float getDist() {
		return dist;
	}

	public void setDist(float dist) {
		this.dist = dist;
	}

	public float getAlt() {
		return alt;
	}

	public void setAlt(float alt) {
		this.alt = alt;
	}

	public float getAlt_acc() {
		return alt_acc;
	}

	public void setAlt_acc(float alt_acc) {
		this.alt_acc = alt_acc;
	}

	public float getBearing() {
		return bearing;
	}

	public void setBearing(float bearing) {
		this.bearing = bearing;
	}

	public float getBearing_acc() {
		return bearing_acc;
	}

	public void setBearing_acc(float bearing_acc) {
		this.bearing_acc = bearing_acc;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getSpeed_acc() {
		return speed_acc;
	}

	public void setSpeed_acc(float speed_acc) {
		this.speed_acc = speed_acc;
	}

	@Override
	public String toString() {
		return "SimulationPoint{" +
				"acc=" + acc +
				", dist=" + dist +
				", alt=" + alt +
				", alt_acc=" + alt_acc +
				", bearing=" + bearing +
				", bearing_acc=" + bearing_acc +
				", speed=" + speed +
				", speed_acc=" + speed_acc +
				'}';
	}
}
