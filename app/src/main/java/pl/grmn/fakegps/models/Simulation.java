package pl.grmn.fakegps.models;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

public class Simulation extends SugarRecord implements Serializable {
    private static final long serialVersionUID = 1L;  // for Serializable interface

    @SerializedName("simulation_id") private String simulationId;
    @SerializedName("simulation_name") private String simulationName;
    @SerializedName("user_id") private String userId;
    @SerializedName("simulation_total_time_s") private String totalTimeSec;
    @SerializedName("simulation_created_time") private String createdTime;
    @SerializedName("simulation_access_code") private String accessCode;
    @SerializedName("track_id") private String trackId;

    @SerializedName("simulation_data") private List<SimulationPoint> simulationPoints;

    public String getSimulationId() {
        return simulationId;
    }

    public void setSimulationId(String simulationId) {
        this.simulationId = simulationId;
    }

    public String getSimulationName() {
        return simulationName;
    }

    public void setSimulationName(String simulationName) {
        this.simulationName = simulationName;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTotalTimeSec() {
        return totalTimeSec;
    }

    public void setTotalTimeSec(String totalTimeSec) {
        this.totalTimeSec = totalTimeSec;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public List<SimulationPoint> getSimulationPoints() {
        return simulationPoints;
    }

    public void setSimulationPoints(List<SimulationPoint> simulationPoints) {
        this.simulationPoints = simulationPoints;
    }

    @Override
    public String toString() {
        return "Simulation{" +
                "simulationId='" + simulationId + '\'' +
                ", simulationName='" + simulationName + '\'' +
                ", userId='" + userId + '\'' +
                ", totalTimeSec='" + totalTimeSec + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", accessCode='" + accessCode + '\'' +
                ", trackId='" + trackId + '\'' +
                ", simulationPoints=" + simulationPoints +
                '}';
    }
}
