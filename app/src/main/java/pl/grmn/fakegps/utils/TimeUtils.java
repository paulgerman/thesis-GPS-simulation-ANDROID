package pl.grmn.fakegps.utils;



public class TimeUtils {
    public static boolean hasDurationPassed(long lastRequestTimeMillis){
        long currentTimeMillis = System.currentTimeMillis();
        long diff = currentTimeMillis - lastRequestTimeMillis;
        long diffMinutes = diff / (60 * 1000) % 60;
        return (diffMinutes >= 30) ? true : false;
    }
}
