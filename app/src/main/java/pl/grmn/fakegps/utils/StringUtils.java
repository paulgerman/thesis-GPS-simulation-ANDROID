package pl.grmn.fakegps.utils;



public class StringUtils {
    public static String checkNull(String baseString) {
        if (baseString == null)
            return "";
        else
            return baseString;
    }

    public static String wordCountFormatter(String baseString) {
        int wordsCount = baseString.split("\\s+").length;
        return (wordsCount < 15) ? baseString : baseString + "...";
    }


}
