package pl.grmn.fakegps.utils;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;



public class MathUtils {
    public static LatLng computeCentroid(List<LatLng> points) {
        double latitude = 0;
        double longitude = 0;
        int n = points.size();

        for (LatLng point : points) {
            latitude += point.latitude;
            longitude += point.longitude;
        }

        return new LatLng(latitude/n, longitude/n);
    }
}
