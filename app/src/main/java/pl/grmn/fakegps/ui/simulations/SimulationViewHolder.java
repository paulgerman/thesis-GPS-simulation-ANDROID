package pl.grmn.fakegps.ui.simulations;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pl.grmn.fakegps.R;



class SimulationViewHolder extends RecyclerView.ViewHolder {
    TextView title, created_at, duration;
    ImageView trackImage;

    SimulationViewHolder(final View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.title);
        created_at = (TextView) itemView.findViewById(R.id.created_at);
        duration = (TextView) itemView.findViewById(R.id.length);
        trackImage = (ImageView) itemView.findViewById(R.id.trackImage);
    }
}
