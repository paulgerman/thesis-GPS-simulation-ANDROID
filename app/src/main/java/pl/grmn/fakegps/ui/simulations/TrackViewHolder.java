package pl.grmn.fakegps.ui.simulations;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import pl.grmn.fakegps.R;



class TrackViewHolder extends RecyclerView.ViewHolder {
    TextView title, created_at, length, seeSimulations;
    ImageView trackImage;
    TableLayout simulationsParent;

    TrackViewHolder(final View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.title);
        created_at = (TextView) itemView.findViewById(R.id.created_at);
        length = (TextView) itemView.findViewById(R.id.length);
        trackImage = (ImageView) itemView.findViewById(R.id.trackImage);
        seeSimulations = (TextView) itemView.findViewById(R.id.seeSimulations);
        simulationsParent = (TableLayout) itemView.findViewById(R.id.simulationsParent);
    }
}
