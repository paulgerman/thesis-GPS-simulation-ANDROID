package pl.grmn.fakegps.ui.simulations;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import pl.grmn.fakegps.R;
import pl.grmn.fakegps.models.Simulation;
import pl.grmn.fakegps.models.Track;
import pl.grmn.fakegps.ui.MainActivity;
import pl.grmn.fakegps.ui.simulationDetails.SimulationDetailsFragment;

import static pl.grmn.fakegps.utils.StringUtils.checkNull;



class SimulationsListAdapter extends RecyclerView.Adapter<SimulationViewHolder> {
    private final Context context;
    private List<Simulation> items;

    SimulationsListAdapter(Context context, List<Simulation> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public SimulationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simulations_list, parent, false);
        return new SimulationViewHolder(listItemView);
    }

    @Override
    public void onBindViewHolder(final SimulationViewHolder holder, int position) {
        final Simulation simulation = items.get(position);
        Track track = null;
        try {
            track = (Track.find(Track.class, "track_id = ?", simulation.getTrackId())).get(0);
        } catch (Exception e) {e.printStackTrace();}
        holder.itemView.setTag(simulation);

        if(track!=null && track.getTrackPreviewURL()!=null && !track.getTrackPreviewURL().equals(""))
            Glide.with(context)
                    .load(track.getTrackPreviewURL())
                    .into(holder.trackImage);

        holder.title.setText(checkNull(simulation.getSimulationName()));
        holder.created_at.setText(checkNull(simulation.getCreatedTime()));
        holder.duration.setText(checkNull(simulation.getTotalTimeSec()) + " secs");

        holder.itemView.setOnClickListener(view -> {
            Fragment f = new SimulationDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("simulation", simulation);
            f.setArguments(bundle);

            ((MainActivity) context).changeFragment(f, true);
        });
    }
}
