package pl.grmn.fakegps.ui.simulations;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import pl.grmn.fakegps.R;
import pl.grmn.fakegps.models.Simulation;
import pl.grmn.fakegps.models.Track;
import pl.grmn.fakegps.ui.MainActivity;
import pl.grmn.fakegps.ui.simulationDetails.SimulationDetailsFragment;

import static pl.grmn.fakegps.utils.StringUtils.checkNull;



class TracksListAdapter extends RecyclerView.Adapter<TrackViewHolder> {
    private final Context context;
    private List<Track> items;

    TracksListAdapter(Context context, List<Track> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public TrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simulations_list, parent, false);
        return new TrackViewHolder(listItemView);
    }

    @Override
    public void onBindViewHolder(final TrackViewHolder holder, int position) {
        final Track track = items.get(position);
        List<Simulation> simulations = null;
        try {
            simulations = (Simulation.find(Simulation.class, "track_id = ?", track.getTrackId()));
        } catch (Exception e) {e.printStackTrace();}
        holder.itemView.setTag(track);
        if(simulations.size()==0)
            holder.itemView.setVisibility(View.GONE);

        if(track!=null && track.getTrackPreviewURL()!=null && !track.getTrackPreviewURL().equals(""))
            Glide.with(context)
                    .load(track.getTrackPreviewURL())
                    .into(holder.trackImage);

        holder.title.setText(checkNull(track.getTrackName()));
        holder.created_at.setText(checkNull(track.getCreatedAt()));
        holder.length.setText(checkNull(track.getTrackLengthMeters()) + "m");

        holder.seeSimulations.setText(String.format("See %d simulations", simulations.size()));

        holder.seeSimulations.setOnClickListener(view -> {
            if(holder.simulationsParent.getVisibility() == View.VISIBLE)
                holder.simulationsParent.setVisibility(View.GONE);
            else
                holder.simulationsParent.setVisibility(View.VISIBLE);
        });

        populateSimulationsTable(holder, simulations, track);
    }

    private void populateSimulationsTable(TrackViewHolder holder, List<Simulation> simulations, Track track){
        holder.simulationsParent.removeAllViews();
        for(Simulation s: simulations) {
            TableRow tableRow = (TableRow) ((Activity)context).getLayoutInflater().inflate(R.layout.row_simulation, null);

            TextView simulationName = (TextView) tableRow.findViewById(R.id.simulationName);
            simulationName.setText(s.getSimulationName());
            TextView simulationDuration = (TextView) tableRow.findViewById(R.id.simulationDuration);
            simulationDuration.setText(s.getTotalTimeSec() + " secs");
            ImageView btnDelete = (ImageView) tableRow.findViewById(R.id.btnDelete);

            btnDelete.setOnClickListener(v -> {
                s.delete();
                this.notifyDataSetChanged();
            });

            simulationName.setOnClickListener(v -> openSimulationDetailsScreen(s, track));
            simulationDuration.setOnClickListener(v -> openSimulationDetailsScreen(s, track));

            holder.simulationsParent.addView(tableRow);
        }
    }

    private void openSimulationDetailsScreen(Simulation s, Track track) {
        Fragment f = new SimulationDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("simulation", s);
        bundle.putSerializable("track", track);
        f.setArguments(bundle);

        ((MainActivity) context).changeFragment(f, true);
    }
}
