package pl.grmn.fakegps.ui.simulations;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.roger.catloadinglibrary.CatLoadingView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.grmn.fakegps.Constant;
import pl.grmn.fakegps.R;
import pl.grmn.fakegps.models.Simulation;
import pl.grmn.fakegps.models.Track;
import pl.grmn.fakegps.providers.ApiInterface;
import pl.grmn.fakegps.ui.MainActivity;
import pl.grmn.fakegps.utils.ConnectivityUtils;
import pl.grmn.fakegps.utils.StringUtils;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.widget.Toast.makeText;



public class SimulationsFragment extends Fragment {
    private Subscription requestSimulationSubscription, requestTrackSubscription;
    private ApiInterface simulationAPI;

    private CatLoadingView loadingView;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.noItemsText) TextView noItemsText;

    AlertDialog alertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simulations, container, false);
        ButterKnife.bind(this, view);

        setHasOptionsMenu(true);
        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if(actionBar!=null)
                actionBar.setTitle("Fake GPS");

        loadingView = new CatLoadingView();

        populateListFromDB();
        initRetrofitProvider();

        return view;
    }

    private void populateListFromDB() {
        List<Track> tracks = Track.listAll(Track.class);
        List<Simulation> simulations = Simulation.listAll(Simulation.class);

        if(tracks!=null && tracks.size()!=0 && simulations!=null && simulations.size()!=0) {
            noItemsText.setVisibility(GONE);
            recyclerView.setVisibility(View.VISIBLE);

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            TracksListAdapter adapter = new TracksListAdapter(getActivity(), tracks);
            recyclerView.setAdapter(adapter);
        } else {
            noItemsText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(GONE);
        }
    }

    private void initRetrofitProvider() {
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxAdapter)
                .build();
        simulationAPI = retrofit.create(ApiInterface.class);
    }

    private void callWSWith(String query) {
        if(ConnectivityUtils.hasInternet(getActivity())) {
            if(loadingView == null) {
                loadingView = new CatLoadingView();
                loadingView.show(getActivity().getSupportFragmentManager(), "");
            }

            requestSimulationSubscription = simulationAPI
                    .getSimulation(query)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            simulationResponse -> {
                                if(simulationResponse.result!=null) {
                                    Simulation wsSimulation = simulationResponse.result;
                                    Simulation existingSimulation = null;
                                    try {
                                        existingSimulation = (Simulation.find(Simulation.class,
                                                "simulation_id = ?", wsSimulation.getSimulationId()))
                                                .get(0);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (existingSimulation != null)
                                        wsSimulation.setId(existingSimulation.getId());
                                    wsSimulation.save();

                                    Track existingTrack = null;
                                    try {
                                        existingTrack = (Track.find(Track.class,
                                                "track_id = ?", wsSimulation.getTrackId()))
                                                .get(0);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (existingTrack == null) {
                                        requestTrackSubscription = simulationAPI
                                                .getTrack(getTrackQueryBy(StringUtils.checkNull(wsSimulation.getTrackId())))
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(trackResponse -> {
                                                    Track wsTrack = trackResponse.result;
                                                    wsTrack.save();

                                                    populateListFromDB();
                                                }, throwable -> {
                                                    throwable.printStackTrace();
                                                    populateListFromDB();
                                                });
                                        if (loadingView != null) loadingView.dismiss();

                                    } else {
                                        populateListFromDB();
                                        if (loadingView != null) loadingView.dismiss();
                                    }
                                }
                            },
                            throwable -> {
                                throwable.printStackTrace();
                                if(loadingView != null) loadingView.dismiss();
                            }
                    );
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.no_internet_title))
                    .setMessage(getString(R.string.no_internet_message))
                    .setPositiveButton(getString(R.string.try_again), (dialog, which) -> {
                        callWSWith(query);
                    })
                    .show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_add:
                showAddSimulationMethodChooser();
                return true;
        }

        return false;
    }

    private void showAddSimulationMethodChooser() {
        alertDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_simulation, null);
        dialogBuilder.setView(dialogView);

        EditText codesTextInput = (EditText) dialogView.findViewById(R.id.codesTextInput);

        TextView btnScan = (TextView) dialogView.findViewById(R.id.btnScan);
        btnScan.setOnClickListener(v -> IntentIntegrator.forSupportFragment(SimulationsFragment.this).initiateScan()); // start scanner

        TextView btnSubmit = (TextView) dialogView.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(v -> {
            if(alertDialog!=null)
                alertDialog.dismiss();
            if(!codesTextInput.getText().toString().equals("")) {
                String[] lines = codesTextInput.getText().toString().split("\n");
                loadingView = null;
                for (String line : lines)
                    callWSWith(getSimulationQueryBy(line.trim()));
            } else
                Toast.makeText(getActivity(), "Text was empty", Toast.LENGTH_SHORT).show();
        }); // submit codes as text input
        dialogBuilder.setCancelable(true);
        alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public void onDestroyView() {
        if(requestSimulationSubscription!=null) requestSimulationSubscription.unsubscribe();
        if(requestTrackSubscription!=null) requestTrackSubscription.unsubscribe();
        super.onDestroyView();
    }

    // get the code scanning results
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Timber.d("scan failed or cancelled");
                makeText(getActivity(), "Scan failed", Toast.LENGTH_SHORT).show();
            } else {
                Timber.d("Scanned: " + result.getContents());

                String obtainedID = result.getContents();
                callWSWith(getSimulationQueryBy(obtainedID));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if(alertDialog!=null)
            alertDialog.dismiss();
    }

    private String getSimulationQueryBy(String simulationId) {
        return "{\"method\":\"simulation_get\",\"params\":[\"" + simulationId + "\",\"simulation_access_code\"]}";
    }

    private String getTrackQueryBy(String trackId) {
        return "{\"method\":\"track_get\",\"params\":[" + trackId + "]}";
    }
}
