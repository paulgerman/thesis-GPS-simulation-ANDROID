package pl.grmn.fakegps.ui.simulationDetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.roger.catloadinglibrary.CatLoadingView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.apptik.widget.MultiSlider;
import pl.grmn.fakegps.Constant;
import pl.grmn.fakegps.MockLocationService;
import pl.grmn.fakegps.R;
import pl.grmn.fakegps.models.Point;
import pl.grmn.fakegps.models.Simulation;
import pl.grmn.fakegps.providers.ApiInterface;
import pl.grmn.fakegps.ui.MainActivity;
import pl.grmn.fakegps.utils.ConnectivityUtils;
import pl.grmn.fakegps.utils.MathUtils;
import pl.grmn.fakegps.utils.StringUtils;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;



public class SimulationDetailsFragment extends Fragment {
    @BindView(R.id.mapView) MapView mapView;
    private GoogleMap googleMap;
    @BindView(R.id.duration) TextView duration;
    @BindView(R.id.rangeSlider) MultiSlider rangeSlider;
    @BindView(R.id.playBtn) ImageView playBtn;
    boolean isCurrentButtonPlay;

    private ApiInterface simulationAPI;
    private Subscription requestTrackSubscription;

    private CatLoadingView loadingView;

    private Simulation simulation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simulation_details, container, false);
        ButterKnife.bind(this, view);

        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        loadingView = new CatLoadingView();
        initRetrofitProvider();


	    Bundle bundle = this.getArguments();
	    Simulation simulation = (Simulation) bundle.getSerializable("simulation");


        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        if(simulation!=null) {
            if(actionBar!=null)
                actionBar.setTitle(simulation.getSimulationName());
            duration.setText(simulation.getTotalTimeSec() + " secs");
        }

	    isCurrentButtonPlay = true;
        // play button press callback
        playBtn.setOnClickListener(v -> {
            if(isCurrentButtonPlay) {
                playBtn.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_pause));
                isCurrentButtonPlay = false;
                startSimulation(this.simulation);

            } else {
                playBtn.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_play));
                isCurrentButtonPlay = true;
	            stopSimulation();
            }
        });


        // progress bar - multislider
        // documentation: https://github.com/apptik/MultiSlider
        rangeSlider.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                if (thumbIndex == 0) {
                    //when the first thumb is modified
                }
            }
        });


        //MAPS INIT AND CALLBACK AFTER FINISHING INIT
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(map -> {
            googleMap = map;
            googleMap.setMyLocationEnabled(true); // For showing a move to my location button

            if(simulation != null )
                callWs(simulation);
        });
        return view;
    }

    // init client http
    private void initRetrofitProvider() {
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxAdapter)
                .build();
        simulationAPI = retrofit.create(ApiInterface.class);
    }

    //fetch simulation
    private void callWs(Simulation simulation) {
        if(ConnectivityUtils.hasInternet(getActivity())) {

            loadingView.show(getActivity().getSupportFragmentManager(), "");
            requestTrackSubscription = simulationAPI
                    .getSimulation(getSimulationQueryBy(StringUtils.checkNull(simulation.getSimulationId())))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(simulationResponse -> {
                        Simulation wsSimulation = simulationResponse.result;
                        this.simulation = wsSimulation;

                        if (wsSimulation != null && wsSimulation.getSimulationPoints() != null && wsSimulation.getSimulationPoints().size() > 0) {
                            List<LatLng> points = new ArrayList<>();
                            for (Point p : wsSimulation.getSimulationPoints()) {
                                LatLng ll = new LatLng(p.getLat(), p.getLng());
                                points.add(ll);
                            }
                            if (points.size() > 0) {
                                googleMap.addPolyline(new PolylineOptions()
                                        .addAll(points)
                                        .width(12)
                                        .color(R.color.black)
                                        .geodesic(true));
                                LatLng center = MathUtils.computeCentroid(points);
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 12));
                            }
                        }
                        if (loadingView != null) loadingView.dismiss();
                    }, throwable -> {
                        throwable.printStackTrace();
                        if (loadingView != null) loadingView.dismiss();
                    });
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.no_internet_title))
                    .setMessage(getString(R.string.no_internet_message))
                    .setPositiveButton(getString(R.string.try_again), (dialog, which) -> {
                        callWs(simulation);
                    })
                    .show();
        }
    }
    //helper pt request:
    private String getSimulationQueryBy(String simulationId) {
        return "{\"method\":\"simulation_get\",\"params\":[" + simulationId + "]}";
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if(requestTrackSubscription!=null) requestTrackSubscription.unsubscribe();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

	public void startSimulation(Simulation simulation){
		Log.v("miau", "starting service?");
		Intent i = new Intent(getActivity(), MockLocationService.class);
		i.putExtra("simulation", simulation);
		getActivity().startService(i);
        Log.v("miau", simulation.toString());
    }

	public void stopSimulation(){
		Log.v("miau", "stopping service?");
		Intent i = new Intent(getActivity(), MockLocationService.class);
		getActivity().stopService(i);
	}
}